<?php

use Illuminate\Support\Str;

if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

if (!defined('TDFM_ROOT')) {
    define('TDFM_ROOT', __DIR__ . DS);
}

if (!function_exists('filesManagerFileById')) {
    function filesManagerImageById($id, $size = null, $webp = false)
    {
        /** @var \TDevAgency\FilesManager\ResourceModels\TdfmFile $filesManager */
        $filesManager = Cache::rememberForever('tdfm_file_by_id_' . $id, function () use ($id) {
            return \TDevAgency\FilesManager\ResourceModels\TdfmFile::find($id);
        });
        if ($filesManager === null) {
            return null;
        }

        if ($webp && $size !== null) {

            $pathInfo = pathinfo($filesManager->original_name);

            if (in_array(Str::lower($pathInfo['extension']), ['jpg', 'jpeg', 'png', 'gif'])) {
                $filesManager->original_name = $pathInfo['filename'] . '.webp';
            }
        }

        if ($size === null) {
            return asset('media/' . $id . '/' . $filesManager->original_name );
        } else {
            return asset('media/' . $id . '/' . $size . '/' . $filesManager->original_name );
        }
    }
}
if (!function_exists('filesManagerPictureById')) {
    function filesManagerPictureById($id, $size = null)
    {
        $size = $size === null ? 'xl' : $size;
        return \View::make('files-manager::picture')->with('picture', \TDevAgency\FilesManager\Facades\PictureTag::pictureTag($id, $size))->render();
    }
}

if (!function_exists('fileManagerActions')) {
    function fileManagerActions()
    {
        return \View::make('files-manager::js_render')->render();
    }
}
