<?php


namespace TDevAgency\FilesManager;


use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use TDevAgency\FilesManager\ResourceModels\TdfmFile;

trait MediaHelper
{
    public function getImage($id, $size = null, $webp = false)
    {
        /** @var TdfmFile $filesManager */
        $filesManager = Cache::rememberForever('tdfm_file_by_id_' . $id, function () use ($id) {
            return TdfmFile::find($id);
        });
        if ($filesManager === null) {
            return null;
        }

        $isSvg = strpos($filesManager->name, '.svg') !== false;

        if (strpos(request()->headers->get('accept'), 'image/webp') !== false && !$isSvg) {
            $webp = true;
            if ($size === null) {
                $size = 'xl';
            }
        }

        if ($webp) {
            $filesManager->original_name = $this->getWebpName($filesManager->original_name);
        }

        if ($size === null) {
            return asset('media/' . $id . '/' . $filesManager->original_name);
        }

        return asset('media/' . $id . '/' . $size . '/' . $filesManager->original_name);
    }

    /**
     * @param $file
     * @return string|null
     */
    private function getWebpName($file)
    {

        $pathInfo = pathinfo($file);

        if (in_array(Str::lower($pathInfo['extension']), ['jpg', 'jpeg', 'png', 'gif'])) {
            return $pathInfo['filename'] . '.webp';
        }

        return null;

    }

    public function getPictureTag($id, $size = null)
    {

        if ($id === null) {
            return null;
        }
        $size = $size === null ? 'xl' : $size;


        /**
         * @var $filesManager TdfmFile
         */
        $filesManager = Cache::rememberForever('tdfm_file_by_id_' . $id, function () use ($id) {
            return TdfmFile::findOrFail($id);
        });

        $filesManager->webpName = $this->getWebpName($filesManager->original_name);
        $tag = [
            'sources' => [],
            'src' => preg_match('/^(\w+)(\.)(svg)+$/', $filesManager->original_name) ? asset('media/original/' . $filesManager->original_name) :
                asset('media/' . $id . '/' . $size . '/' . $filesManager->original_name)
        ];
        if (preg_match('/^image\/(jpe?g|gif|png)+$/', $filesManager->file_mime)) {
            $tag['sources'][] = [
                'src' => asset('media/' . $id . '/' . $size . '/' . $filesManager->webpName),
                'mime' => 'image/webp',
            ];
            $tag['sources'][] = [
                'src' => asset('media/' . $id . '/' . $size . '/' . $filesManager->original_name),
                'mime' => $filesManager->file_mime,
            ];
        }

        return View::make('files-manager::picture')->with('picture', $tag)->render();
    }

}
