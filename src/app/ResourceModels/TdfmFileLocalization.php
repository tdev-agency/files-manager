<?php

namespace TDevAgency\FilesManager\ResourceModels;

use Illuminate\Database\Eloquent\Model;

class TdfmFileLocalization extends Model
{
    protected $fillable = [
        'lang',
        'tdfm_file_id',
        'file_title',
        'file_title',
        'file_alt',
        'file_description',
    ];

}
