<?php

namespace TDevAgency\FilesManager\Http\Controllers;

use claviska\SimpleImage;
use Exception;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use TDevAgency\FilesManager\Facades\FmImage;
use TDevAgency\FilesManager\Facades\PictureTag;
use TDevAgency\FilesManager\ResourceModels\TdfmFile;

class FilesManagerControllerApi extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $savedFiles = [];
        try {
            DB::transaction(function () use ($request, &$savedFiles) {
                if ($request->hasFile('files')) {
                    $files = $request->file('files');
                    /**
                     * @var UploadedFile $file
                     */
                    foreach ($files as $file) {
                        $filesManager = new TdfmFile();
                        $filesManager->name = Storage::disk('tdfm_original')->put('/', $file);
                        if (pathinfo($filesManager->name, \PATHINFO_EXTENSION) === '' && $file->getClientOriginalExtension() === 'svg') {
                            $newName = sprintf('%s.%s', $filesManager->name, $file->getClientOriginalExtension());
                            Storage::disk('tdfm_original')->move($filesManager->name, $newName);
                            $filesManager->name = $newName;
                        }
                        $savedFiles[] = $filesManager->name;
                        $filesManager->original_name = Str::slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)) . '.' . pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
                        $filesManager->file_mime = $file->getClientMimeType();
                        $filesManager->file_size = $file->getSize();
                        $filesManager->save();

                        if (preg_match('/^image\/(jpe?g|gif|png)+$/', $file->getClientMimeType())) {
                            /** @var SimpleImage $imgage */
                            $this->generateSizes($file, $filesManager);
                        }
                        Artisan::call('fm:symlinks', ['--id' => $filesManager->id]);

                    }
                }
            });


            return $this->index($request);
        } catch (\Throwable $e) {
            if (!empty($savedFiles)) {
                foreach ($savedFiles as $file) {
                    if (Storage::disk('tdfm_original')->exists($file)) {
                        Storage::disk('tdfm_original')->delete($file);
                    }
                }
            }
            Log::error($e);
            return JsonResponse::create(App::environment() === 'local' ? [$e->getMessage()] : [], 500);
        }
    }

    /**
     * @param UploadedFile $file
     * @param TdfmFile $tdfmFile
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    private function generateSizes(UploadedFile $file, TdfmFile $tdfmFile)
    {
        $name = preg_replace('/(\w+)(.)(\w+)/', '${1}', $tdfmFile->name);
        $newFile = clone $file;
        foreach (Config::get('tdfm.sizes') as $key => $size) {
            $image = FmImage::fromString($newFile->get());

            $width = $image->getWidth() < $size ? $image->getWidth() : $size;
            if ($key !== 'xs') {
                $image->resize($width);
            } else {
                $image->thumbnail($width, $width, 'center');
            }
            $image->autoOrient();


            Storage::disk('tdfm_generated')->put(
                sprintf('%s/%s/%s.%s', $tdfmFile->id, $key, $name, $file->getClientOriginalExtension()),
                $image->toString($file->getClientMimeType(), Config::get('tdfm.quality'))
            );

            Storage::disk('tdfm_generated')->put(
                sprintf('%s/%s/%s.%s', $tdfmFile->id, $key, $name, 'webp'),
                $image->toString('image/webp', Config::get('tdfm.quality'))
            );
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $attachments = TdfmFile::sortable()->orderByDesc('created_at');
        if (request()->has('q')) {
            $attachments = $attachments->where('original_name', 'like', '%' . request()->query('q') . "%");
        }

        $attachments = $attachments->paginate($request->get('limit') ?? 24);

        return JsonResponse::create($attachments);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @param Request $request
     * @return JsonResponse
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function destroy($id, Request $request)
    {
        $tdfm = Config::get('tdfm');
        $count = 0;
        foreach ($tdfm['models_with_images'] as $modelName => $row) {
            foreach ($row as $rowModel => $field) {
                $query = $modelName::where($field, '=', $id)->get();
                $count += count($query);
            }
        }
        if ($count !== 0) {
            return JsonResponse::create(['count' => $count]);
        }
        $tdfmFile = TdfmFile::findOrFail($id);

        try {
            $fileName = $tdfmFile->name;
            $fileId = $tdfmFile->id;
            $tdfmFile->delete();
            Storage::disk('tdfm_generated')->deleteDir(DS . $fileId);
            Storage::disk('tdfm_original')->delete($fileName);
            Cache::delete('tdfm_file_by_id_' . $id);


            if (file_exists(public_path('media/' . $tdfmFile->id))) {
                File::deleteDirectory(public_path('media/' . $tdfmFile->id), true);
            }

            return JsonResponse::create(['count' => $count]);
        } catch (\Throwable $e) {
            Log::error($e);
            return JsonResponse::create($request->all(), 500);
        } catch (Exception $e) {
            Log::error($e);
            return JsonResponse::create($request->all(), 500);
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function update($id, Request $request)
    {
        if (isset($id) && $request->has('original_name')) {
            $tdfmFile = TdfmFile::findOrFail($id);
            try {


                $tdfmFile->original_name =Str::slug(pathinfo($request->get('original_name'), PATHINFO_FILENAME)) . '.' . pathinfo($request->get('original_name'), PATHINFO_EXTENSION);
                $tdfmFile->update();


                Artisan::call('fm:symlinks', ['--id' => $tdfmFile->id]);

                Cache::delete('tdfm_file_by_id_' . $id);
                return JsonResponse::create([]);
            } catch (\Throwable $e) {
                Log::error($e);
                return JsonResponse::create($request->all(), 500);
            } catch (\Exception $e) {
                Log::error($e);
                return JsonResponse::create($request->all(), 500);
            }
            return JsonResponse::create([]);
        } else {
            return JsonResponse::create(['error' => 'no id or original_name']);
        }
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function show(Request $request)
    {
        if (!$request->has('id')) {
            return abort(404);
        }
        $size = !$request->has('size') ? 'xl' : $request->get('size');

        $response = PictureTag::pictureTag($request->get('id'), $size);
        return JsonResponse::create($response);
    }
}
