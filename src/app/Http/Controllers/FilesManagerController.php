<?php

namespace TDevAgency\FilesManager\Http\Controllers;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use TDevAgency\FilesManager\Facades\FmImage;
use TDevAgency\FilesManager\ResourceModels\TdfmFile;

class FilesManagerController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $fs;

    public function __construct(Filesystem $fs)
    {
        $this->fs = $fs;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $size
     * @param $file
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function image($size, $id, $file)
    {

        if (!config('tdfm.generate_webp') && preg_match('/^(\w+)(.webp)$/', $file)) {
            return Response::make('', 404);
        }
        $tdfmFile = Cache::rememberForever('tdfm_file_by_id_' . $id, function () use ($id) {
            return TdfmFile::where('id', $id)->firstOrFail();
        });
        if ($tdfmFile !== null) {
            $path = sprintf('%s' . DS . '%s' . DS . '%s' . DS . '%s', Config::get('tdfm.disks.tdfm_generated.root'), $id, $size, $tdfmFile->name);
            /** @var File $fileString */
            if ($this->fs->exists($path)) {
            } elseif (Storage::disk('tdfm_original')->exists($tdfmFile->name)) {
                $this->generateSizes($tdfmFile);
            } else {
                return response(null, 404);
            }
            $expires = Carbon::now()->addMonth(6);
            $modified = Carbon::createFromTimestamp($this->fs->lastModified($path));
            return Response::make($this->fs->get($path), 200, ['Content-Type' => $this->fs->mimeType($path)])
                ->setMaxAge(15552000)
                ->setLastModified($modified)
                ->setExpires($expires);
        }
        return response(null, 404);
    }

    /**
     * @param TdfmFile $tdfmFile
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    private function generateSizes(TdfmFile $tdfmFile)
    {
        $name = preg_replace('/(\w+)(.)(\w+)/', '${1}', $tdfmFile->name);
        $ext = preg_replace('/(\w+)(.)(\w+)/', '${3}', $tdfmFile->name);
        foreach (Config::get('tdfm.sizes') as $key => $size) {
            $image = FmImage::fromString(Storage::disk('tdfm_original')->get($tdfmFile->name));

            $width = $image->getWidth() < $size ? $image->getWidth() : $size;
            if ($key !== 'xs') {
                $image->resize($width);
            } else {
                $image->thumbnail($width, $width);
            }
            $image->autoOrient();
            Storage::disk('tdfm_generated')->put(
                sprintf('%s/%s/%s.%s', $tdfmFile->id, $key, $name, $ext),
                $image->toString($tdfmFile->file_mime, Config::get('tdfm.quality'))
            );

            if (Config::get('tdfm.generate_webp')) {
                Storage::disk('tdfm_generated')->put(
                    sprintf('%s/%s/%s.%s', $tdfmFile->id, $key, $name, 'webp'),
                    $image->toString('image/webp', Config::get('tdfm.quality'))
                );
            }
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $size
     * @param $file
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function originalFile($id, $file)
    {
        if (!config('tdfm.generate_webp') && preg_match('/^(\w+)(.webp)$/', $file)) {
            return Response::make('', 404);
        }
        /** @var TdfmFile $tdfmFile */
        $tdfmFile = Cache::rememberForever('tdfm_file_by_id_' . $id, function () use ($id) {
            return TdfmFile::where('id', $id)->firstOrFail();
        });
        if ($tdfmFile !== null) {
            $path = sprintf('%s' . DS . '%s', Config::get('tdfm.disks.tdfm_original.root'), $tdfmFile->name);
            /** @var File $fileString */
            if ($this->fs->exists($path)) {
            } elseif (Storage::disk('tdfm_original')->exists($tdfmFile->name)) {
//                $this->generateSizes($tdfmFile);
            } else {
                return response(null, 404);
            }
            $expires = Carbon::now()->addMonth(6);
            $modified = Carbon::createFromTimestamp($this->fs->lastModified($path));
            return Response::make($this->fs->get($path), 200, ['Content-Type' => $tdfmFile->file_mime])
                ->setMaxAge(15552000)
                ->setLastModified($modified)
                ->setExpires($expires);
        }
        return Response::make('', 404);
    }

    /**
     * @return mixed
     */
    public function js()
    {
        $js = View::make('files-manager::js')->render();
        $modified = Carbon::createFromTimestamp($this->fs->lastModified(TDFM_ROOT . 'resources' . DS . 'views' . DS . 'js.blade.php'));
        $expires = Carbon::now()->addMonth(6);
        return Response::make($js, 200, ['Content-Type' => 'application/javascript'])
            ->setMaxAge(15552000)
            ->setLastModified($modified)
            ->setExpires($expires);
    }

    /**
     * @param $path1
     * @param null $path2
     * @return mixed
     */
    public function svg_image($path1, $id, $path2 = null)
    {
        $svgName = $path2 === null ? $path1 : $path2;

        $tdfmFile = Cache::rememberForever('tdfm_file_by_id_' . $id, function () use ($id) {
            return TdfmFile::where('id', $id)->firstOrFail();
        });

        $path = sprintf('%s' . DS . '%s', Config::get('tdfm.disks.tdfm_original.root'), $tdfmFile->name);

        $expires = Carbon::now()->addMonth(6);
        $modified = Carbon::createFromTimestamp($this->fs->lastModified($path));
        return Response::make($this->fs->get($path), 200, ['Content-Type' => 'image/svg+xml'])
            ->setMaxAge(15552000)
            ->setLastModified($modified)
            ->setExpires($expires);
    }

}
