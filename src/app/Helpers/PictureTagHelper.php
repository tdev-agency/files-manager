<?php

namespace TDevAgency\FilesManager\Helpers;


use Illuminate\Support\Facades\Cache;
use TDevAgency\FilesManager\ResourceModels\TdfmFile;

class PictureTagHelper
{
    public function pictureTag($id, $size = 'xl')
    {

        $filesManager = Cache::rememberForever('tdfm_file_by_id_' . $id, function () use ($id) {
            return TdfmFile::findOrFail($id);
        });

        $tag = [
            'sources' => [],
            'src' => parse_url(asset('media/' . $filesManager->id . '/' . $filesManager->original_name), PHP_URL_PATH)
        ];
        if (preg_match('/^image\/(jpe?g|gif|png|webp)+$/', $filesManager->file_mime)) {
            $pathInfo = pathinfo($filesManager->original_name);

            if ($pathInfo['extension'] !== 'webp') {
                $tag['sources'][] = [
//                'src' =>  route('tdfm.image', ['file' => $filesManager->original_name, 'size' => $size, 'id' => $filesManager->id]),
                    'src' => parse_url(asset('media/' . $filesManager->id . '/' . $size . '/' . $pathInfo['filename'] . '.webp'), PHP_URL_PATH),
                    'mime' => 'image/webp',
                ];
            }
            $tag['sources'][] = [
                'src' => parse_url(asset('media/' . $filesManager->id . '/' . $size . '/' . $filesManager->original_name), PHP_URL_PATH),
                'mime' => $filesManager->file_mime,
            ];
        }
        return $tag;
    }
}
