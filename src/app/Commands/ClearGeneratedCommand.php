<?php
/**
 * Created by PhpStorm.
 * User: andrii.trush
 * Date: 20/07/2018
 * Time: 11:43
 */

namespace TDevAgency\FilesManager\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;


class ClearGeneratedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tdfm:clear:generated';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $directories = Storage::disk('tdfm_generated')->directories();
        foreach($directories as $dir) {
            Storage::disk('tdfm_generated')->deleteDirectory($dir);
        }
    }
}
