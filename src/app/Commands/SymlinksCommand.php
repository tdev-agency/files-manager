<?php

namespace TDevAgency\FilesManager\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use TDevAgency\FilesManager\ResourceModels\TdfmFile;

class SymlinksCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fm:symlinks {--id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if ($this->hasOption('id') && $this->option('id') !== null) {
            $files = TdfmFile::where('id', $this->option('id'))->get();
            if ($files->isNotEmpty()) {

                $files->each(function ($file) {
//                    if (File::isDirectory(public_path('media/' . $file->id))) {
//                        File::deleteDirectory(public_path('media/' . $file->id));
//                    }
                });
            }
        } else {
            $files = TdfmFile::get();

            if (File::isDirectory(public_path('media'))) {
                File::deleteDirectory(public_path('media'));
            }
        }

        if ($files->isEmpty()) {
            return;
        }

        $sizes = array_keys(Config::get('tdfm.sizes'));

        if (!File::isDirectory(public_path('media'))) {
            File::makeDirectory(public_path('media'));
            chmod(public_path('media'), 0775);
        }

        /** @var TdfmFile $file */
        foreach ($files as $file) {
            if ($file->original_name === $file->name) {
                continue;
            }

//            continue if file physically doesn't exists on the server
            if (!Storage::disk('tdfm_original')->exists($file->name)) {
                continue;
            }


            if (!File::isDirectory(public_path('media/' . $file->id))) {
                File::makeDirectory(public_path('media/' . $file->id));
                chmod(public_path('media/' . $file->id), 0775);
            }

            $pathInfo = pathinfo($file->original_name);
            $pathInfoCached = pathinfo($file->name);
            if (!File::exists(public_path('media/' . $file->id . '/' . $file->original_name))) {
                File::link(Storage::disk('tdfm_original')->path($file->name), public_path('media/' . $file->id . '/' . $file->original_name));
            }
            if (Storage::disk('tdfm_generated')->exists($file->id)) {


                $webpName = $pathInfoCached['filename'] . '.webp';
                $newWebpName = $pathInfo['filename'] . '.webp';

                // TODO SVG
                foreach ($sizes as $size) {
                    if (!Storage::disk('tdfm_generated')->exists($file->id . '/' . $size)) {

                    } else {

                        if (!File::isDirectory(public_path('media/' . $file->id . '/' . $size))) {
                            File::makeDirectory(public_path('media/' . $file->id . '/' . $size));
                            chmod(public_path('media/' . $file->id . '/' . $size), 0775);
                        }

                        if (!File::exists(public_path('media/' . $file->id . '/' . $size . '/' . $file->original_name))) {
                            File::link(Storage::disk('tdfm_generated')->path($file->id . '/' . $size . '/' . $file->name), public_path('media/' . $file->id . '/' . $size . '/' . $file->original_name));
                        }
                        if (Storage::disk('tdfm_generated')->exists($file->id . '/' . $size . '/' . $webpName)) {
                            if (!File::exists(public_path('media/' . $file->id . '/' . $size . '/' . $newWebpName))) {
                                File::link(Storage::disk('tdfm_generated')->path($file->id . '/' . $size . '/' . $webpName), public_path('media/' . $file->id . '/' . $size . '/' . $newWebpName));
                            }
                        }
                    }
                }
            }
        }
    }
}
