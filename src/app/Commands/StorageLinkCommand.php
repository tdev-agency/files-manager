<?php
/**
 * Created by PhpStorm.
 * User: andrii.trush
 * Date: 20/07/2018
 * Time: 11:43
 */

namespace TDevAgency\FilesManager\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;


class StorageLinkCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tdfm:storage:link';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if(file_exists(public_path('js/tdfm'))) {
            $this->error('The "public/tdfm" directory already exists.');
        } else {
            File::link(
                TDFM_ROOT . DS . 'resources'. DS . 'tdfm' . DS . 'dist',
                public_path('js/tdfm')
            );
            File::link(
                TDFM_ROOT . DS . 'resources'. DS . 'tdfm' . DS . 'dist',
                resource_path('assets/js/vendor/tdfm')
            );

            $this->info('The [public/tdfm] directory has been linked.');
        }
    }
}
