<?php
/**
 * Created by PhpStorm.
 * User: andrii
 * Date: 18.01.19
 * Time: 11:48
 */

namespace TDevAgency\FilesManager;


trait PictureTag
{
    public function pictureTag($id, $size = 'xl')
    {

        $filesManager = Cache::rememberForever('tdfm_file_by_id_' . $id, function () use ($id) {
            return TdfmFile::findOrFail($id);
        });

        $tag = [
            'sources' => [],
            'src' => preg_match('/^(\w+)(.)(svg)+$/', $filesManager->name) ? route('tdfm.original_file',['id' => $filesManager->id ,'slug' => $filesManager->original_name]) : route('tdfm.image', ['file' => $filesManager->original_name, 'id' => $filesManager->id, 'size' => $size])
        ];
        if (preg_match('/^image\/(jpe?g|gif|png)+$/', $filesManager->file_mime)) {
            $name = preg_replace('/(\w+)(.)(\w+)/', '${1}', $filesManager->name);
            $tag['sources'][] = [
                'src' => route('tdfm.image', ['file' => $name . '.webp', 'size' => $size]),
                'mime' => 'image/webp',
            ];
            $tag['sources'][] = [
                'src' => route('tdfm.image', ['file' => $filesManager->original_name, 'id'=> $filesManager->id,'size' => $size]),
                'mime' => $filesManager->file_mime,
            ];
        }
        return $tag;
    }

}
