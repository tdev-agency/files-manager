<?php

use Illuminate\Support\Facades\Route;
use TDevAgency\FilesManager\Http\Controllers\FilesManagerController;
use TDevAgency\FilesManager\Http\Controllers\FilesManagerControllerApi;
use TDevAgency\FilesManager\Http\Middleware\LowercaseRoutes;

Route::prefix('fm')->group(function () {
    Route::get('{size}/{id}/{file?}', [FilesManagerController::class, 'svg_image'])->name('tdfm.image')
        ->where([
            'file' => '^(.*)(\.)(svg)+$',
            'id' => '^\d+$',
            'size' => '^((xs|sm|md|lg|xl|4k))+$'
        ]);
    Route::get('{size}/{id}/{file}', [FilesManagerController::class, 'image'])->name('tdfm.image')
        ->where([
            'file' => '^(.*)(\.)(?i)(jpe?g|gif|png|webp)+$',
            'id' => '^\d+$',
            'size' => '^(xs|sm|md|lg|xl|4k)+$'
        ]);

    Route::get('{id}/{slug}', [FilesManagerController::class, 'originalFile'])->name('tdfm.original_file')
        ->where(['name' => '^(\w+)(.)(\w+)$']);
});
Route::get('files-manager/show', [FilesManagerControllerApi::class, 'show'])->name('files-manager.show');
Route::apiResource('files-manager', FilesManagerControllerApi::class)->only(['index', 'store', 'destroy', 'update']);
Route::get('files-manager-actions.js', [FilesManagerController::class, 'js'])->name('files-manager.js');
