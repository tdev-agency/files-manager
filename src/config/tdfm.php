<?php
return [
    'disks' => [
        'tdfm_original' => [
            'driver' => env('TDFM_DRIVER', 'local'),
            'root' => storage_path('app' . DS . 'tdfm' . DS . 'original'),
            'url' => env('TDFM_URL', '/fm'),
            'visibility' => 'public',
        ],
        'tdfm_generated' => [
            'driver' => env('TDFM_DRIVER', 'local'),
            'root' => storage_path('app' . DS . 'tdfm' . DS . 'generated'),
            'url' => env('TDFM_URL', '/fm'),
            'visibility' => 'public',
        ]
    ],


    // Sizes of generated images

    'sizes' => [
        '4k' => env('TDFM_SIZE_4K', 3840),
        'xl' => env('TDFM_SIZE_XL', 1920),
        'lg' => env('TDFM_SIZE_LG', 1200),
        'md' => env('TDFM_SIZE_MD', 768),
        'sm' => env('TDFM_SIZE_SM', 576),
        'xs' => env('TDFM_SIZE_XS', 150), // is thumb 150*150
    ],

// Specify in which models images exists

//    'models_with_images' => [
//        Post::class => [
//            'image_id'
//        ]
//    ],

    'models_with_images' => [],

    // Allow to generate WEBP image

    'generate_webp' => env('TDFM_GET_WEBP', true),

    'quality' => env('TDFM_QUALITY', 85),

    'lazy' => env('TDFM_LAZY', true)
];
